import React from 'react'

import { Link } from 'react-router-dom';

const Header = () => {
    return (
        <>
            <header id="header" className="fixed-top ">
                <div className="container d-flex align-items-center justify-content-lg-between">
                    <Link to="/" className="logo me-auto me-lg-0"><img src="assets/img/logo/M_logo3.jpg" alt="" className="img-fluid" /></Link>
                    <nav id="navbar" className="navbar order-last order-lg-0">
                        <ul>
                            <li><Link className="nav-link scrollto" to="/Home">About</Link></li>
                            <li><Link className="nav-link scrollto" to="/Home">Data</Link></li>
                            <li className="dropdown"><Link to="/Home"><span>Drop Down</span> <i className="bi bi-chevron-down"></i></Link>
                                <ul>
                                    <li><Link to="/Home">Drop Down 1</Link></li>
                                    <li className="dropdown"><Link to="/Home"><span>Deep Drop Down</span> <i
                                        className="bi bi-chevron-right"></i></Link>
                                        <ul>
                                            <li><Link to="/Home">Deep Drop Down 1</Link></li>
                                            <li><Link to="/Home">Deep Drop Down 2</Link></li>
                                            <li><Link to="/Home">Deep Drop Down 3</Link></li>
                                            <li><Link to="/Home">Deep Drop Down 4</Link></li>
                                            <li><Link to="/Home">Deep Drop Down 5</Link></li>
                                        </ul>
                                    </li>
                                    <li><Link to="/Home">Drop Down 2</Link></li>
                                    <li><Link to="/Home">Drop Down 3</Link></li>
                                    <li><Link to="/Home">Drop Down 4</Link></li>
                                </ul>
                            </li>
                            <li><Link className="nav-link scrollto" to="/Home">Help</Link></li>
                        </ul>
                        
                        {/* <i className="bi bi-list mobile-nav-toggle"></i> */}
                    </nav>
                </div>
            </header>
        </>

    )
}

export default Header