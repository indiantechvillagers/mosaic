import React from 'react'
import { Link } from "react-router-dom"

const Signin = () => {
  return (
    <div><main>
      <div className="container">

        <section className="section register min-vh-100 d-flex flex-column align-items-center justify-content-center py-4">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-lg-4 col-md-6 d-flex flex-column align-items-center justify-content-center">

                <div className="d-flex justify-content-center py-4">
                  <Link to="/" className="logo d-flex align-items-center w-auto">
                    <img src="assets/img/logo/M_logo5.jpg" style={{ marginTop: "-3%", margiLeft: "-5px" }} alt="" />
                    <span className="d-none d-lg-block">Mosaic</span>
                  </Link>
                </div>
                {/* <!-- End Logo --> */}

                <div className="card mb-3">

                  <div className="card-body">

                    <div className="pt-4 pb-2">
                      <h5 className="card-title text-center pb-0 fs-4">Sign in</h5>
                      <p className="text-center small">Need an account?<span><Link to="">Get
                        started</Link></span></p>
                    </div>

                    <form className="row g-3 needs-validation" noValidate>

                      <div className="col-12">
                        <label htmlFor="yourUsername" className="form-label">Email</label>
                        <div className="input-group has-validation">
                          <span className="input-group-text" id="inputGroupPrepend">@</span>
                          <input type="text" name="username" className="form-control" id="yourUsername" required />
                          <div className="invalid-feedback">Please enter your username.</div>
                        </div>
                      </div>

                      <div className="col-12">
                        <label htmlFor="yourPassword" className="form-label">Password</label>
                        <input type="password" name="password" className="form-control" id="yourPassword" required />
                        <div className="invalid-feedback">Please enter your password!</div>
                      </div>

                      <div className="col-12">
                        <div className="form-check">
                          <input className="form-check-input" type="checkbox" name="remember" value="true" id="rememberMe" style={{ backgroundColor: "#2f6d8a" }} />
                          <label className="form-check-label" htmlFor="rememberMe">Remember me</label>
                        </div>
                      </div>
                      <div className="col-12">
                        <Link className="btn btn-primary w-100" style={{
                          color: "#fff",
                          backgroundColor: "#2f6d8a"
                        }} to="/Home">Sign in</Link>
                      </div>
                      <div className="col-12">
                        <p className="small mb-0" style={{ textAlign: "center" }}> <span><Link to="">Forgot
                          Password? </Link></span> </p>
                        <p className="small mb-0" style={{ textAlign: "center" }}>Don't rceive confirmation instructions? </p>
                      </div>
                    </form>

                  </div>
                </div>
              </div>
            </div>
          </div>

        </section>

      </div>
    </main></div>
  )
}

export default Signin