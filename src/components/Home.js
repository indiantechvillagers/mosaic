import React from 'react';
import { Link } from "react-router-dom";
import Header from './Header';
const Home = () => {

  return (

    <div>
      <Header />
      <section id="hero" className="d-flex align-items-center justify-content-center">
        <div className="container">
          <div className="row justify-content-md-center">
            <div className="col-md-8" style={{ backgroundColor: "#fff", padding: "26px 17px 20px" }}>
              <h4> <strong>
                Welcome to <span style={{ color: "rgb(255,0,58)" }}>MOSAIC</span>
              </strong>
              </h4>
              <p style={{ color: "#000", fontSize: "18px", textAlign: "justify" }}> The <span style={{ color: "rgb(255,0,58)" }}>MOSAIC</span> data iPORTAL aspires to be a <strong>single</strong> point-of-entry for both business and technical users of all levels serving as a comprehensive resource for finding and accessing
                all reporting, dashboarding and data analytics assets.
              </p>
            </div>
          </div>
          <div className="row justify-content-center" data-aos="fade-up" data-aos-delay="150">
            <div className="col-xl-6 col-lg-8">

            </div>
          </div>
          <div className="row gy-4 mt-5 justify-content-center" data-aos="zoom-in" data-aos-delay="250">
            <div className="col-xl-2 col-md-4">
              <div className="icon-box">
                <img src="./assets/img/icon/cup-of-hot-chocolate.png" alt="" />
                <h3><Link to="/Nonclinical">Non-Clinical</Link></h3>
              </div>
            </div>
            <div className="col-xl-2 col-md-4">
              <div className="icon-box">
                <img src="./assets/img/icon/bank.png" alt="" />
                <h3><Link to="">Data Scientist</Link></h3>
              </div>
            </div>
            <div className="col-xl-2 col-md-4">
              <div className="icon-box">
                <img src="./assets/img/icon/health-clinic.png" alt="" />
                <h3><Link to="">Population Health</Link></h3>
              </div>
            </div>
            <div className="col-xl-2 col-md-4">
              <div className="icon-box">
                <img src="./assets/img/icon/car.png" alt="" />
                <h3><Link to="">PI Research</Link></h3>
              </div>
            </div>
            <div className="col-xl-2 col-md-4">
              <div className="icon-box">
                <img src="./assets/img/icon/users.png" alt="" />
                <h3><Link to="">Data Analyst</Link></h3>
              </div>
            </div>
          </div>
          <div className="row gy-4 mt-2 justify-content-center" data-aos="zoom-in" data-aos-delay="250">
            <div className="col-xl-2 col-md-4">
              <div className="icon-box">
                <img src="./assets/img/icon/online-education.png" alt="" />
                <h3><Link to="">Executive Manager</Link></h3>
              </div>
            </div>
            <div className="col-xl-2 col-md-4">
              <div className="icon-box">
                <img src="./assets/img/icon/leaf.png" alt="" />
                <h3><Link to="">Researcher</Link></h3>
              </div>
            </div>
            <div className="col-xl-2 col-md-4">
              <div className="icon-box">
                <img src="./assets/img/icon/home.png" alt="" />
                <h3><Link to="">Power User</Link></h3>
              </div>
            </div>
            <div className="col-xl-2 col-md-4">
              <div className="icon-box">
                <img src="./assets/img/icon/wrench.png" alt="" />
                <h3><Link to="">Statistician</Link></h3>
              </div>
            </div>
            <div className="col-xl-2 col-md-4">
              <div className="icon-box">
                <img src="./assets/img/icon/gallery.png" alt="" />
                <h3><Link to="">Business Analyst</Link></h3>
              </div>
            </div>
          </div>
        </div>
      </section>
      
    </div>

  )
}

export default Home