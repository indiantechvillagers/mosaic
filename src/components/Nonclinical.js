import React from 'react'
import { Link } from "react-router-dom";

const Nonclinical = () => {
    return (
        <div>
            <i className="bi bi-list mobile-nav-toggle d-xl-none"></i>

            <header id="header_copy">
                <div class="d-flex flex-column">

                    <div class="profile">
                   
                    <Link to="/Home"><img src="assets/img/logo/M_logo3.jpg" alt="" /></Link>
                    </div>

                    <div class="card">
                        <div class="card-body" style={{ color: "#72C1FA" }}>
                            <div class="accordion accordion-flush" id="accordionFlushExample">
                                <div class="accordion-item">
                                    <h2 class="accordion-header" id="flush-headingOne">
                                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                            Report Access
                                        </button>
                                    </h2>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingTwo">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                                Report Data
                                            </button>
                                        </h2>
                                    </div>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingThree">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                                Report Popularity
                                            </button>
                                        </h2>
                                    </div>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingFour">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                                Report History
                                            </button>
                                        </h2>
                                    </div>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingFive">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                                Report Licence
                                            </button>
                                        </h2>
                                    </div>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingSix">
                                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                                iHelp?
                                            </button>
                                        </h2>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <main id="main-2">
                <section id="home" className="d-flex align-items-center justify-content-center mt-3">
                    <div className="container">
                        <div className="row justify-content-center" data-aos="fade-up" data-aos-delay="150">
                            <div className="col-xl-12 col-lg-8">
                                <h3 style={{ color: "rgb(46,66,181)", fontSize: 42 }}> <strong>Mosaic Non-Clinical Operator Persona Page Deisen </strong> </h3>
                            </div>
                        </div>
                        <div className="row gy-4 mt-2  justify-content-center" data-aos="fade-up" data-aos-delay="250">
                            <div className="col-xl-2 col-md-4">
                                <div className="icon-box">
                                    <h3><Link to="/Iportalnonclinical">Search</Link></h3>
                                </div>
                            </div>
                            <div className="col-xl-2 col-md-4">
                                <div className="icon-box">
                                    <h3><Link to="">Top Reports</Link></h3>
                                </div>
                            </div>
                            <div className="col-xl-2 col-md-4">
                                <div className="icon-box">
                                    <h3><Link to="">Help?</Link></h3>
                                </div>
                            </div>
                            <div className="col-xl-2 col-md-4">
                                <div className="icon-box">
                                    <h3><Link to="">Report Metadata</Link></h3>
                                </div>
                            </div>
                        </div>
                        <div className="row gy-4 mt-2 justify-content-center" data-aos="zoom-in" data-aos-delay="250">
                            <div className="col-xl-2 col-md-4">
                                <div className="icon-box">
                                    <h3><Link to="">Populer Search</Link></h3>
                                </div>
                            </div>
                            <div className="col-xl-2 col-md-4">
                                <div className="icon-box">
                                    <h3><Link to="">Report Metrics</Link></h3>
                                </div>
                            </div>
                            <div className="col-xl-2 col-md-4">
                                <div className="icon-box">
                                    <h3><Link to="">Most Downloaded</Link></h3>
                                </div>
                            </div>
                            <div className="col-xl-2 col-md-4">
                                <div className="icon-box">
                                    <h3><Link to="">Meta Data</Link></h3>
                                </div>
                            </div>
                        </div>
                        <div className="row gy-4 mt-2 justify-content-center" data-aos="zoom-in" data-aos-delay="250">
                            <div className="col-xl-2 col-md-4">
                                <div className="icon-box">
                                    <h3><Link to="">Report Access</Link></h3>
                                </div>
                            </div>
                            <div className="col-xl-2 col-md-4">
                                <div className="icon-box">
                                    <h3><Link to="">Data Relationships</Link></h3>
                                </div>
                            </div>
                            <div className="col-xl-2 col-md-4">
                                <div className="icon-box">
                                    <h3><Link to="">Report Accuracy Check</Link></h3>
                                </div>
                            </div>
                            <div className="col-xl-2 col-md-4">
                                <div className="icon-box">
                                    <h3><Link to="">Collaboration</Link></h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </main>
        </div>
    )
}
export default Nonclinical