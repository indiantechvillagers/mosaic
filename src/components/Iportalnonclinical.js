import React from 'react'
import { Link } from "react-router-dom";

const Iportalnonclinical = () => {
  return (
    <div>

<i className="bi bi-list mobile-nav-toggle d-xl-none"></i>


      <header id="header_copy">
        <div className="d-flex flex-column" />

        <div className="profile">
          
           <Link to="/Home"><img src="assets/img/logo/download.png" alt="" /></Link>
        </div>

        <div className="card">
          <div className="card-body" style={{ color: "#72C1FA" }}>

            <div className="accordion accordion-flush" id="accordionFlushExample">
              <div className="accordion-item">
                <h2 className="accordion-header" id="flush-headingOne">
                  <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                    Report Access
                  </button>
                </h2>
                <div className="accordion-item">
                  <h2 className="accordion-header" id="flush-headingTwo">
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                      Report Data
                    </button>
                  </h2>
                </div>
                <div className="accordion-item">
                  <h2 className="accordion-header" id="flush-headingThree">
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                      Report Popularity
                    </button>
                  </h2>
                </div>
                <div className="accordion-item">
                  <h2 className="accordion-header" id="flush-headingFour">
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                      Report History
                    </button>
                  </h2>
                </div>
                <div className="accordion-item">
                  <h2 className="accordion-header" id="flush-headingFive">
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                      Report Licence
                    </button>
                  </h2>
                </div>
                <div className="accordion-item">
                  <h2 className="accordion-header" id="flush-headingSix">
                    <button className="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                      iHelp?
                    </button>
                  </h2>
                </div>
              </div>

            </div>
          </div>
        </div>
      </header>


      <main id="main-2">
        <div className="container">
        <div class="row mt-3 justify-content-end">
                <div class="col-xl-12 col-lg-8">
                    <h3 class="h3"> <strong>MOSAIC iPORTAL NON-CLINICAL OPRATION PERSONA </strong> </h3>
                </div>
                <div class="col-10">
                    <p>Find Data</p>
                    <div class="search">
                        <i class="fa fa-search"></i>
                        <input type="text" class="form-control"/>
                        <button class="btn btn-primary">Search</button>
                    </div>
                    <div class="btn-groups">
                        <button class="btn-sm dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                              <strong> Search Tips</strong>
                        </button>
                        <ul class="dropdown-menu">
                            ...
                        </ul>
                    </div>
                </div>
                <div class="col-2">
                    <p class="text-decoration-underline mt-5" style={{ cursor: "pointer" }}>view all</p>

                </div>
            </div>
        </div>
        <section class="section">
            <div class="row mt-5">
                <div class="col-lg-4">

                    <div class="card1">
                        <div class="card-body1">

                            <h5 class="card-title"> <i class="bi-eyeglasses"></i>&#160; Browse</h5>
                            <p class="bottom">Topics/Series/Thematic date Collections</p>
                            <hr class="hr" />
                            <ul>
                                <li class="bottom">
                                    <p> List studies for which online analysis is </p>
                                </li>
                                <hr class="hr" />
                                <li class="bottom">
                                    <p> List self published data in cluiding reqlication</p>
                                </li>
                                <hr class="hr" />
                                <li class="bottom">
                                    <p> list studics with learning guides</p>
                                </li>
                                <hr class="hr" />
                            </ul>
                            <p> <strong>New/Updated Date Releases</strong> </p>
                            <ul>
                                <li class="bottom">
                                    <p> In the last week</p>
                                </li>
                                <hr/>
                                <li class="bottom">
                                    <p> In the last month</p>
                                </li>
                                <hr/>
                                <li class="bottom">
                                    <p> In the last quartar</p>
                                </li>
                                <hr/>
                                <li class="bottom">
                                    <p> In the last year</p>
                                </li>
                                <hr/>

                            </ul>
                            <p class="bottom">Browse by subject term</p>
                            <hr/>
                        </div>
                    </div>
                </div>
                <div className="col-lg-4">

                    <div className="card1">
                        <div className="card-body1">

                            <h5 className="card-title"> <i className="fa fa-bar-chart" aria-hidden="true"></i>&#160;Statistics</h5>
                            <figure className="bottom">
                                <img src="./assets/img/logo/database.png" alt=""/>
                                <p className="figure1 bottom"> <strong>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;17,124 Studies</strong> </p>
                            </figure>
                            <hr/>
                            <br/>
                            <br/>
                            <figure className="bottom">
                                <img src="./assets/img/logo/braces.png" alt=""/>
                                <p className="figure bottom"> <strong>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;5,948,134 Variables</strong> </p>
                            </figure>
                            <hr/>
                            <br/><br/>
                            <figure className="bottom">
                                <img src="./assets/img/logo/book.png" alt=""/>
                                <p className="figure2 bottom"> <strong>&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;102,738 Publications</strong> </p>
                            </figure>
                            <hr/>

                        </div>
                    </div>
                </div>
                <div className="col-lg-4">

                    <div className="card1">
                        <div className="card-body1">

                            <h5 className="card-title"> <i className="fa fa-search" aria-hidden="true"></i> &#160;Most Popular Search Terms</h5>
                          
                            <figure>
                                <img className="figure1" src="./assets/img/Screenshot (44).png" alt=""/>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row mt-4">
                <div className="col-lg-4">
                    <div className="card1">
                        <div className="card-body1">
                            <h5 className="card-title"><i className="fa fa-download"></i>&#160;Most Download</h5>
                            <div style={{ overflow: "auto", height: 259 }}>
                                <p className="font-size">1. National Longitudinal Study of Adolescent to Adult</p>
                                <hr/>
                                <p className="font-size">&#160;&#160;&#160; Health(Add Health),1994-2008[Public Use]</p>
                                <hr/>
                                <p className="font-size">2. List studies for which online analysis is available</p>
                                <hr/>
                                <p className="font-size">3. List studies for which online analysis is available</p>
                                <hr/>
                                <p className="font-size">&#160;&#160;&#160; Health(Add Health),1994-2008[Public Use]</p>
                                <hr/>
                                <p className="font-size">4. List studies for which online analysis is available</p>
                                <hr/>
                                <p className="font-size">5. List studies for which online analysis is available</p>
                                <hr/>
                                <p className="font-size">&#160;&#160;&#160; Health(Add Health),1994-2008[Public Use]</p>
                                <hr/>
                                <p className="font-size">6. List studies for which online analysis is available</p>
                                <hr/>
                                <p className="font-size">&#160;&#160;&#160; Health(Add Health),1994-2008[Public Use]</p>
                                <hr/>
                                <p className="font-size">7. List studies for which online analysis is available</p>
                                <hr/>
                                <p className="font-size">8. List studies for which online analysis is available</p>
                                <hr/>
                                <p className="font-size">9. List studies for which online analysis is available</p>
                                <hr/>
                                <p className="font-size">&#160;&#160;&#160; Health(Add Health),1994-2008[Public Use]</p>
                                <hr/>
                                <p className="font-size">10. List studies for which online analysis is available</p>
                                <hr/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="col-lg-4">

                    <div className="card1">
                        <div className="card-body1">

                            <h5 className="card-title"><i className="fa fa-hand-paper-o"></i>&#160;Key Metrics Reports</h5>
                            <p style={{ textAlign: "justify", fontSize: 13 }}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum, quibusdam. Lorem ipsum,</p>
                            <br/>
                            <p style={{ textAlign: "justify", fontSize: 13 }}>Lorem ipsum dolor sit amet consectetur adipisicing elit. Alias esse nesciunt praesentium voluptatem odio illo exercitationem ullam modi dolor rem blanditiis aut et, quam accusamus, fugit, rerum quis quo perspiciatis. Vero libero
                                magni dolorum delectus, eius aperiam deleniti quae accusamus,</p>
                            <p className="bottom text-decoration-underline mt-5" style={{ color: "#2849CE", cursor: "pointer" }}><strong>More Information</strong></p>
                            <hr/>
                        </div>
                    </div>
                </div>
                <div className="col-lg-4">

                    <div className="card1">
                        <div className="card-body1">

                            <h5 className="card-title"><i className="fa fa-globe"></i>&#160;Dept Using Our Data</h5>
                            <p className="bottom">1. Onoolory</p>
                            <hr/>
                            <p className="bottom">2. OutPatinet Departmemt</p>
                            <hr/>
                            <p className="bottom">3. Radiolocy</p>
                            <hr/>
                            <p className="bottom">4. Medical Reports</p>
                            <hr/>
                            <p className="bottom">5. Pharmacy Dept</p>
                            <hr/>
                            <p className="bottom">6. Neurology Dept</p>
                            <hr/>
                        </div>
                    </div>
                </div>
            </div>
        </section>
       
      </main>
     
    </div >
  )
}

export default Iportalnonclinical