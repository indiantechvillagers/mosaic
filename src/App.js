import { Routes, Route, } from "react-router-dom";

import Signin from "./components/Signin";
import Home from "./components/Home";
import Nonclinical from "./components/Nonclinical";
import Iportalnonclinical from "./components/Iportalnonclinical";

function App() {
  return (
    <div>
      
      <Routes>
        <Route path="/" element={<Signin />} />
        <Route path="home" element={<Home />} />
        <Route path="nonclinical" element={<Nonclinical />} />
        <Route path="iportalnonclinical" element={<Iportalnonclinical />} />
        
        
      </Routes>
    </div>
  );
}

export default App;
